#include <bits/stdc++.h>

using namespace std;


bool coinChanging(int *a, int *s, int m, int n)
{
	for(int i = 0; i < m; i++)
		s[i] = 0;
	
	int i = 0;
	
	while(i < m && n > 0){
		s[i] = n / a[i];
		n = n - s[i] * a[i];
		i++;
	}
	
	if(n > 0) return false;
	return true;
}

int main(){
	int m = 4, n = 15;
	int *a = new int[m]{10, 5, 2, 1};
	int *s = new int[m];
	
	cout<<"So tien: "<< n <<endl;
	if(coinChanging(a, s, m, n)) {
		for(int i = 0; i < m; i++)
			if(s[i] > 0) 
				cout<<"Lay " << s[i] <<" so "<<a[i]<<endl;
	} else {
		cout<<"Khong co cach nao"<<endl;
	}
	
	return 0;
}
