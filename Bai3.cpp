#include <bits/stdc++.h>

using namespace std;
	
struct Quat {
	string tenHSX;
	string mauSac;
	double giaBan;
};

void print(Quat *d, int n) {
	cout<<left<<setw(15)<<"Ten Hang"<<setw(10)<<"Gia ban"<<endl;
	for(int i = 0; i <n; i++)
		cout<<left<<setw(15)<<d[i].tenHSX<<setw(10)<<d[i].giaBan<<endl;	
}

void mergeSort(Quat *a, int l, int r) {
    if (r > l) {
        int m = (l + r) / 2;
        mergeSort(a, l, m);
        mergeSort(a, m + 1, r);

        Quat b[r + 1];
        for (int i = l; i <= m; i++) {
            b[i] = a[i];
        }
        for (int j = m + 1; j <= r; j++) {
            b[r + m + 1 - j] = a[j];
        }

        // Tron mang b vao mang a
        int i = l;
        int j = r;
        for (int k = l; k <= r; k++) {
            if (b[i].giaBan >  b[j].giaBan) {
            	//b[i] > b[j]: giam dan
                a[k] = b[i];
                i++;
            } else {
                a[k] = b[j];
                j--;
            }
        }
    }
}

int thamLam(Quat *a,  int p, int n) {
	int i = 0;
	int cnt = 0;
	
	while(i < n && p > 0) {
		p = p - a[i].giaBan;
		cnt ++;
		i++;
	}
	
	if(p < 0)	return cnt;
	return 0;
}

int main()
{
	int p = 1000000;
	int n = 6;
	Quat *d = new Quat[n];
	d[0] = {"Toshiba",   "Red",    200000};
	d[1] = {"Panasonic", "Green",  300000};
	d[2] = {"Samsung",   "Yellow", 100000};
	d[3] = {"LG",        "Brown",  600000};
	d[4] = {"Vin",       "Blue",   400000};
	d[5] = {"DongA",     "Black",  500000};

	mergeSort(d, 0, n - 1);
	print(d, n);
	
	int kq = thamLam(d, p, n);
	if(kq > 0 ) {
		cout<<"Mua duoc nhieu nhat "<<kq<<" so quat"<<endl;
		print(d, kq);		
	}		
	else {
		cout<<"No"<<endl;
	}
	
return 0;
}
